create table migrations
(
    id        int unsigned auto_increment
        primary key,
    migration varchar(255) not null,
    batch     int          not null
)
    collate = utf8_unicode_ci;

create table users
(
    id         int auto_increment
        primary key,
    name       varchar(200)                        null,
    family     varchar(200)                        null,
    username   varchar(200)                        null,
    password   varchar(200)                        null,
    created_at timestamp default CURRENT_TIMESTAMP not null,
    updated_at timestamp                           null,
    deleted_at timestamp                           null,
    user_id    int                                 not null
);

create table campaigns
(
    id         int auto_increment
        primary key,
    body       varchar(500)                        null,
    title      varchar(200)                        null,
    group_id   int                                 null,
    created_at timestamp default CURRENT_TIMESTAMP not null,
    updated_at timestamp                           null,
    deleted_at timestamp                           null,
    user_id    int                                 not null,
    constraint f2
        foreign key (user_id) references users (id)
            on update cascade on delete cascade
);

create index campaignindex
    on campaigns (user_id);

create table contacts
(
    id         int auto_increment
        primary key,
    user_id    int                                 null,
    categoryId int                                 null,
    name       varchar(200)                        null,
    created_at timestamp default CURRENT_TIMESTAMP not null,
    updated_at timestamp                           null,
    deleted_at timestamp                           null,
    category   varchar(200)                        null,
    constraint fk17
        foreign key (user_id) references contacts (id)
            on update cascade on delete cascade,
    constraint fk4
        foreign key (user_id) references users (id)
            on update cascade on delete cascade
);

create table campaigns_contacts
(
    id          int auto_increment
        primary key,
    campaign_id int                                 not null,
    persons_id  int                                 not null,
    created_at  timestamp default CURRENT_TIMESTAMP not null,
    updated_at  timestamp                           null,
    deleted_at  timestamp                           null,
    constraint f5
        foreign key (campaign_id) references campaigns (id)
            on update cascade on delete cascade,
    constraint f6
        foreign key (persons_id) references contacts (id)
            on update cascade on delete cascade
);

create index campaign2index
    on campaigns_contacts (campaign_id);

create index person2index
    on campaigns_contacts (persons_id);

create index personindex
    on contacts (user_id);

create index persons2index
    on contacts (user_id);

create table domains
(
    id         int auto_increment
        primary key,
    name       varchar(200)                        null,
    created_at timestamp default CURRENT_TIMESTAMP not null,
    updated_at timestamp                           null,
    deleted_at timestamp                           null,
    user_id    int                                 not null,
    constraint f1
        foreign key (user_id) references users (id)
            on update cascade on delete cascade
);

create index domainindex
    on domains (user_id);


