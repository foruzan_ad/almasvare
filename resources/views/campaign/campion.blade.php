@extends("template.header-footer")

@section("content")


    <div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
        <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

            <!-- begin:: Subheader -->
            <div class="kt-subheader   kt-grid__item" id="kt_subheader">
                <div class="kt-container ">
                    <div class="kt-subheader__main">
                        <h3 class="kt-subheader__title">
                            Add campaion </h3>
                        <span class="kt-subheader__separator kt-hidden"></span>

                    </div>

                </div>
            </div>

            <!-- end:: Subheader -->

        </div>
    </div>


    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
											<span class="kt-portlet__head-icon">
												<i class="kt-font-brand flaticon2-line-chart"></i>
											</span>
                    <h3 class="kt-portlet__head-title">
                        What Campaign That I have
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <div class="kt-portlet__head-actions">

                            &nbsp;
                            <a href="#kt_modal" class="btn btn-brand btn-elevate btn-icon-sm"
                               data-toggle="modal">
                                <i class="la la-plus"></i>
                                New Campaign
                            </a>
                        </div>
                    </div>
                </div>

            </div>
            <div class="kt-portlet__body">

                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="table">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Title</th>
                        <th>Body</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>

                <!--end: Datatable -->
            </div>
        </div>
    </div>



    <!--begin::Modal-->
    <div class="modal fade" id="kt_modal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">New Campaign</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="col-12">



                            <div class="form-group">
                                <label for="input-title"
                                       class="form-control-label">Title</label>
                                <input class="form-control" id="input-title"></input>
                            </div>
                            <div class="form-group">
                                <label for="input-title"
                                       class="form-control-label">Body</label>
                                <input class="form-control" id="input-body"></input>
                            </div>




                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="add()">Submit</button>
                </div>
            </div>
        </div>
    </div>

    <!--end::Modal-->
    <!--begin::updateModal-->
    <div class="modal fade" id="kt_modal_update" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">New Campaign</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="col-12">



                            <div class="form-group">
                                <label for="input-title-update"
                                       class="form-control-label">Title</label>
                                <input class="form-control" id="input-title-update"></input>
                            </div>
                            <div class="form-group">
                                <label for="input-title"
                                       class="form-control-label">Body</label>
                                <input class="form-control" id="input-body-update"></input>
                            </div>





                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="sendUpdateRequest()">Submit</button>
                </div>
            </div>
        </div>
    </div>

    <!--end::updateModal-->


    <!-- end:: Page -->


    <script src="{{@asset("plugin/plugins.bundle.js")}}" type="text/javascript"></script>
    <script src="{{@asset("js/scripts.bundle.js")}}" type="text/javascript"></script>
    <script src="{{@asset("js/datatables.bundle.js")}}" type="text/javascript"></script>
    <script src="{{@asset("js/notify-config.js")}}" type="text/javascript"></script>


    <script src="{{@asset("js/campaign.js")}}" type="text/javascript"></script>


@endsection
