@extends("template.header-footer")

@section("content")

    <div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
        <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

            <!-- begin:: Subheader -->
            <div class="kt-subheader   kt-grid__item" id="kt_subheader">
                <div class="kt-container ">
                    <div class="kt-subheader__main">
                        <h3 class="kt-subheader__title">
                            Category Person </h3>
                        <span class="kt-subheader__separator kt-hidden"></span>

                    </div>

                </div>
            </div>

            <!-- end:: Subheader -->

        </div>
    </div>
    <!-- start:: service -->

    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
											<span class="kt-portlet__head-icon">
												<i class="kt-font-brand flaticon2-line-chart"></i>
											</span>
                    <h3 class="kt-portlet__head-title">
                        What Person do
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <div class="kt-portlet__head-actions">

                            &nbsp;
                            <a href="#kt_modal" class="btn btn-brand btn-elevate btn-icon-sm"
                               data-toggle="modal">
                                <i class="la la-plus"></i>
                                New Person's Category
                            </a>
                        </div>
                    </div>
                </div>

            </div>
            <div class="kt-portlet__body">

                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="table">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Category</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>

                <!--end: Datatable -->
            </div>
        </div>
    </div>


    <!-- end:: category portfolio -->
    <!--begin::Modal-->
    <div class="modal fade" id="kt_modal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">New Person's Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">


                            <div class="col-9 ">
                                <div class="form-group">
                                    <label for="input-name"
                                           class="form-control-label">Category</label>
                                    <input type="text" class="form-control" id="input-category">
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="add()">Submit</button>
                </div>
            </div>
        </div>
    </div>

    <!--end::Modal-->

    <!--begin::updateModal-->
    <div class="modal fade" id="kt_modal_update" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">New Person's Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">


                            <div class="col-9 ">
                                <div class="form-group">
                                    <label for="input-name-update"
                                           class="form-control-label">Category</label>
                                    <input type="text" class="form-control" id="input-category-update">
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="sendUpdateRequest()">Submit</button>
                </div>
            </div>
        </div>
    </div>

    <!--end::updateModal-->


    <!-- end:: Page -->


    <script src="{{@asset("plugin/plugins.bundle.js")}}" type="text/javascript"></script>
    <script src="{{@asset("js/scripts.bundle.js")}}" type="text/javascript"></script>
    <script src="{{@asset("js/datatables.bundle.js")}}" type="text/javascript"></script>
    <script src="{{@asset("js/notify-config.js")}}" type="text/javascript"></script>


    <script src="{{@asset("js/categoryaddperson.js")}}" type="text/javascript"></script>



@endsection
