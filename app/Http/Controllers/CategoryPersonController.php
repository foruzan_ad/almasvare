<?php


namespace App\Http\Controllers;


use App\Model\Person;
use App\Repository\addpersonRepo;
use Illuminate\Http\Request;

class CategoryPersonController extends Controller
{
    private $categoryPersonRepo;

    /**
     * CategoryPersonController constructor.
     * @param $categoryPersonRepo
     */
    public function __construct()
    {
        $this->categoryPersonRepo=new addpersonRepo();
    }

    public function index(Request $request)
    {

         return view('person.categoryPerson');

    }
    public function getRecord(Request $request)
    {
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');

        $data=$this->categoryPersonRepo->getItem(['id','category']);

        $jason_data=array(
            "draw"=>$request->input('draw'),
            "recordsTotal"=>count($data),
            "recordsFiltered"=>count($data),
            "data"=>$data
        );
//        dd($jason_data);
        echo json_encode($jason_data);

    }

    public function addRecord(Request $request)
    {
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');

        $addCategoryperson=new Person();
        $addCategoryperson->name=null;
        $addCategoryperson->category=$request->input('category');
        $addCategoryperson->categoryId=$addCategoryperson->id;
        $issave=$addCategoryperson->save();
        if($issave){
            echo json_encode("ok");
        }
        else{
            echo json_encode("nok");
        }


    }

    public function UpdateRecord(Request $request)
    {
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');
        $addCategoryperson=$this->categoryPersonRepo->find($request->input('id'));
        $addCategoryperson->category=$request->input('category');
        $issave=$addCategoryperson->save();
        if($issave){
            echo json_encode("update ok.\n");
        }
        echo json_encode("update nok.\n");

    }

    public function deleteRecord(Request $request)
    {
        $id=$request->input('id');
        $isdelete=$this->categoryPersonRepo->deleteById($id);
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');
        if($isdelete){
            echo json_encode("delete.\n");
        }
        echo json_encode("no delete.\n");

    }



}
