<?php


namespace App\Http\Controllers;



use App\Model\campaign;
use App\Repository\campaignRepo;
use Illuminate\Http\Request;

class campaignController extends Controller
{
    private $campaignrepo;

    /**
     * campaignController constructor.
     * @param $campaignrepo
     */
    public function __construct()
    {
        $this->campaignrepo = new campaignRepo();
    }

    public function index(Request $request)
    {
        return view('campaign.campion');

    }
    public function getRecord(Request $request)
    {
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');
        $data=$this->campaignrepo->getItem(['id','body','title']);

        $jason_data=array(
            "draw"=>$request->input('draw'),
            "recordsTotal"=>count($data),
            "recordsFiltered"=>count($data),
            "data"=>$data
        );
//        dd($jason_data);
        echo json_encode($jason_data);

    }

    public function addRecord(Request $request)
    {
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');


        $body=$request->input('body');
        $user_id=1;
        $title=$request->input('title');

        $campaign=new campaign();
        $campaign->body=$body;
        $campaign->user_id=$user_id;
        $campaign->title=$request->input('title');

        $issave=$campaign->save();
        if($issave){
            echo json_encode("ok");
        }
        else{
            echo json_encode("nok");
        }


    }

    public function UpdateRecord(Request $request)
    {
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');
        $campaign=$this->campaignrepo->find($request->input('id'));
        $campaign->title=$request->input('title');
        $campaign->body=$request->input('body');
        $issave=$campaign->save();
        if($issave){
            echo json_encode("update ok.\n");
        }
        echo json_encode("update nok.\n");

    }

    public function deleteRecord(Request $request)
    {
        $id=$request->input('id');
        $isdelete=$this->campaignrepo->deleteById($id);
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');
        if($isdelete){
            echo json_encode("delete.\n");
        }
        echo json_encode("no delete.\n");

    }


}
