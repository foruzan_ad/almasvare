<?php


namespace App\Http\Controllers;



use App\Model\Domain;
use App\Repository\domainRepo;
use Illuminate\Http\Request;

class DomainController extends Controller
{
    private $domainRepo;

    /**
     * DomainController constructor.
     * @param $domainRepo
     */
    public function __construct()
    {
        $this->domainRepo = new domainRepo();
    }

    public function index(Request $request)
    {
        return view('domain.domain');

    }
    public function getRecord(Request $request)
    {
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');
        $data=$this->domainRepo->getItem(['id','name']);

        $jason_data=array(
            "draw"=>$request->input('draw'),
            "recordsTotal"=>count($data),
            "recordsFiltered"=>count($data),
            "data"=>$data
        );
//        dd($jason_data);
        echo json_encode($jason_data);

    }

    public function addRecord(Request $request)
    {
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');

        $name=$request->input('name');
        $user_id=1;
        $domain=new Domain();
        $domain->name=$name;
        $domain->user_id=$user_id;


        $issave=$domain->save();
        if($issave){
            echo json_encode("ok");
        }
        else{
            echo json_encode("nok");
        }


    }

    public function UpdateRecord(Request $request)
    {
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');
        $domain=$this->domainRepo->find($request->input('id'));
        $domain->name=$request->input('name');
        $issave=$domain->save();
        if($issave){
            echo json_encode("update ok.\n");
        }
        echo json_encode("update nok.\n");

    }

    public function deleteRecord(Request $request)
    {
        $id=$request->input('id');
        $isdelete=$this->domainRepo->deleteById($id);
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');
        if($isdelete){
            echo json_encode("delete.\n");
        }
        echo json_encode("no delete.\n");

    }



}
