<?php


namespace App\Repository;


use App\Model\Person;
use phpDocumentor\Reflection\Types\Self_;

class addpersonRepo extends BaseRepo
{
    public static $model=Person::class;

    public function getItem(array $attr)
    {
        return Self::$model::
            whereNotNull('name')->
            whereNotNull('category')->
            get($attr)->toArray();

    }

    public function getcategoryItem(array $attr)
    {
        return self::$model::
        whereNull('name')->
            get($attr)->toArray();
    }
}
