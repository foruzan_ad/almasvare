<?php


namespace App\Repository;


class BaseRepo
{
    public static $model;
    public function insert($data) {
        return static::$model::insert($data);
    }

    public function find($id) {
        return static::$model::find($id);
    }
    public function update($data) {
        return static::$model::update($data);
    }

    public function all() {
        return static::$model::all()
            ->toArray();
    }
    public function deleteById($data)
    {
        return static::$model::destroy($data);

    }

}
