
let rowData;
let table;

jQuery(document).ready(function () {

    initTable();
    $(document).on('click', '#delete', function () {
        rowData = table.row($(this).parents('tr')).data();
        remove(rowData)
    });
    $(document).on('click', '#edit', function () {
        rowData = table.row($(this).parents('tr')).data();
        openUpdateModal(rowData);
    });

    $('#kt_modal').on('hidden.bs.modal', function () {
        clearModal(true);
    });

    $('#kt_modal_update').on('hidden.bs.modal', function () {
        clearModal(false);
    });

});
function initTable() {
    table=$('#table').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: 'http://almasvare.com/api/campaign',
        columns: [
            {
                "render": function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                },
                "width": "5%"
            },

            {data: 'body'},
            {data: 'title'},

            {
                data: null,
                title: 'Actions'
            }
        ],
        columnDefs: [

            {
                targets: 3,
                title: "Actions",
                orderable: false,
                render: function (data, type, full, meta) {
                    return `
                        <button id="edit" class="flaticon2-file btn btn-sm btn-clean btn-icon btn-icon-sm"
                                                title="Edit details"></button>
                               <button id="delete" class=" flaticon2-delete btn btn-sm btn-clean btn-icon btn-icon-md"
                                                title="Delete"></button>`;
                }
            },
            {
                className: "dt-center", "targets": "_all"
            }
        ]

    });
};

let getDataFromInputs=function (isInsert) {

    let params;
    let body;
    let id;
    let title;


    if(isInsert){

    body=$('#input-body').val();
    title=$('#input-title').val();
    }
    else {
        id=rowData.id;
        body=$('#input-body-update').val();
        title=$('#input-title-update').val();
    }
    params={
        id:id,
        body:body,
        title:title


};
    return params;


};

function add() {
    let requestParams =getDataFromInputs(true);
    axios.get('/api/add/campaign', {
        params: requestParams
    })
        .then(function (response) {
            console.log(response);
            showSuccessNotification();
            table.ajax.reload();
            clearModal();

        })
        .catch(function (error) {
            console.log(error);
            showErrorNotification();

        })
        .then(function () {
            closeModal('#kt_modal');
        });

}
function remove(data) {

    swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!'
    }).then(function (result) {
        if (result.value) {


            axios.get('/api/delete/campaign', {
                params: {
                    id: data.id
                }
            })
                .then(function (response) {
                    console.log(response);


                    swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    );


                    table.ajax.reload();


                })
                .catch(function (error) {
                    console.log(error);

                    showErrorNotification();

                }).then(function () {
                // always executed
            });


        }


    });

}


function sendUpdateRequest() {

    let params=getDataFromInputs(false);
    axios.get('/api/update/campaign', {
        params: params
    })
        .then(function (response) {
            console.log(response);
            showSuccessNotification();
            table.ajax.reload();
            clearModal(false);

        })
        .catch(function (error) {
            console.log(error);
            showErrorNotification();

        })
        .then(function () {
            closeModal('#kt_modal_update');
        });

}
function openUpdateModal(data) {

    $('#input-title-update').val(data.title);
    $('#input-body-update').val(data.body);
    $('#kt_modal_update').modal('toggle');

}
function clearModal(isInsert) {
if(isInsert){
    $('#input-title');
    $('#input-body')

}
else {
    $('#input-title-update');
    $('#input-body-update')


}

}

function closeModal(id) {
  $(id).modal('toggle');
}
