"use strict";

// Class definition

let showSuccessNotification = function () {
    let successNotifyConfig = {
        type: 'success',
        allow_dismiss: false,
        newest_on_top: true,
        mouse_over: false,
        showProgressbar: false,
        spacing: 10,
        timer: 2000,
        placement: {
            from: 'bottom',
            align: 'right'
        },
        offset: {
            x: 30,
            y: 30
        },
        delay: 1000,
        z_index: 1000,
        animate: {
            enter: 'animated ' + 'fadeIn',
            exit: 'animated ' + 'fadeOut'
        }
    };

    let content = {
        'title': 'Done',
        'message': 'New Item is added successfully'

    };
    $.notify(content, successNotifyConfig);
};

let showErrorNotification = function () {

    let errorNotifyConfig= {
        type: 'danger',
        allow_dismiss: false,
        newest_on_top: true,
        mouse_over: false,
        showProgressbar: false,
        spacing: 10,
        timer: 2000,
        placement: {
            from: 'bottom',
            align: 'right'
        },
        offset: {
            x: 30,
            y: 30
        },
        delay: 1000,
        z_index: 1000,
        animate: {
            enter: 'animated ' + 'fadeIn',
            exit: 'animated ' + 'fadeOut'
        }
    };


    let content = {
        'title': 'Error',
        'message': 'Somethings are going wrong!'

    };


    $.notify(content, errorNotifyConfig);
};

let showUpdateNotification = function () {

    let  uploadNotifyConfig = {
        type: 'primary',
        icon:'icon la la-cloud-upload',
        allow_dismiss: false,
        newest_on_top: true,
        mouse_over: false,
        showProgressbar: true,
        spacing: 10,
        timer: 1000,
        placement: {
            from: 'bottom',
            align: 'right'
        },
        offset: {
            x: 30,
            y: 30
        },
        delay: 5000,
        z_index: 1000,
        animate: {
            enter: 'animated ' + 'fadeIn',
            exit: 'animated ' + 'fadeOut'
        }
    };

    let content = {
        'message': 'Preparing...'

    };

    let uploadNotify = $.notify(content, uploadNotifyConfig);
    uploadNotify.update('icon', 'icon la la-cloud-upload');

    return uploadNotify;
};