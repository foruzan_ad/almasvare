<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/home',function (){
    return view('admin.index');});

//person
Route::get('/addperson',function (){
    return view('person.addperson');});
Route::get('/admin/addperson','PersonController@index');

//APIperson
Route::get('/api/add/addperson','PersonController@addRecord');
Route::get('/api/addperson','PersonController@getRecord');
Route::get('/api/delete/addperson','PersonController@deleteRecord');
Route::get('/api/update/addperson','PersonController@UpdateRecord');




//categoryperson
Route::get('/categoryperson',function (){
    return view('person.categoryPerson');});
Route::get('/admin/categoryperson','CategoryPersonController@index');

//APIcategoryperson
Route::get('/api/add/categoryperson','CategoryPersonController@addRecord');
Route::get('/api/categoryperson','CategoryPersonController@getRecord');
Route::get('/api/delete/categoryperson','CategoryPersonController@deleteRecord');
Route::get('/api/update/categoryperson','CategoryPersonController@UpdateRecord');

//domain
Route::get('/domain',function (){
    return view('domain.domain');});
Route::get('/admin/domain','DomainController@index');

//APIdomain
Route::get('/api/add/domain','DomainController@addRecord');
Route::get('/api/domain','DomainController@getRecord');
Route::get('/api/delete/domain','DomainController@deleteRecord');
Route::get('/api/update/domain','DomainController@UpdateRecord');



//campaign
Route::get('/campaign',function (){
    return view('campaign.campion');});
Route::get('/admin/campaign','campaignController@index');

//APIcampaign
Route::get('/api/add/campaign','campaignController@addRecord');
Route::get('/api/campaign','campaignController@getRecord');
Route::get('/api/delete/campaign','campaignController@deleteRecord');
Route::get('/api/update/campaign','campaignController@UpdateRecord');

